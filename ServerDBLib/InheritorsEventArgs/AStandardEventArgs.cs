﻿using System;
using System.Runtime.Serialization;

namespace InheritorsEventArgs
{
    [DataContract]
    public abstract class AStandardEventArgs : EventArgs
    {
        [DataMember]
        public abstract string GetMessage { get; protected set; }
    }
}
