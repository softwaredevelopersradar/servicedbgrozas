﻿using InheritorsEventArgs;
using System;

namespace ClientDataBase
{
    public interface ITableUpdate<T> where T : GrozaSModelsDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArgs<T>> OnUpTable;
    }
}
