﻿using InheritorsEventArgs;
using System;

namespace ClientDataBase
{
    public interface ITableAddRange<T> where T : GrozaSModelsDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArgs<T>> OnAddRange;
    }
}
