﻿using System;
using System.Collections.Generic;
using ClientDataBase.ServiceDB;
using System.Text.RegularExpressions;
using GrozaSModelsDBLib;
using InheritorsEventArgs;
using DataEventArgs = InheritorsEventArgs.DataEventArgs;
using OperationTableEventArgs = InheritorsEventArgs.OperationTableEventArgs;
using System.Threading.Tasks;

namespace ClientDataBase
{
    public partial class ClientDB : IServiceDBCallback
    {
        private int ID;
        private string name = "";
        private string endpointAddress = "net.tcp://127.0.0.1:8302/";
        private ServiceDBClient ClientServiceDB;

        /// <summary>
        /// словарь, который хранит NameTable - имя таблицы, 
        /// ITableAction - объект класса, который реализует интерфейс(ITableAction - действия над таблицей) 
        /// </summary>
        public readonly Dictionary<NameTable, IClassTables> Tables = new Dictionary<NameTable, IClassTables>
        {
            { NameTable.TableJammerStation, new ClassTable<TableJammerStation>() },
            { NameTable.GlobalProperties, new ClassTable<GlobalProperties>() },
            { NameTable.TablePattern, new ClassTablePattern() },

            { NameTable.TableSectorsRecon, new ClassInheritTableAsp<TableSectorsRecon>() },
            { NameTable.TableFreqForbidden, new ClassInheritTableAsp<TableFreqForbidden>() },
            { NameTable.TableFreqKnown, new ClassInheritTableAsp<TableFreqKnown>() },
            { NameTable.TableFreqRangesRecon, new ClassInheritTableAsp<TableFreqRangesRecon>() },

            { NameTable.TableSource, new ClassTable<TableSource>() },
            { NameTable.TableTrack, new ClassTable<TableTrack>() },
            { NameTable.TableJamBearing, new ClassTable<TableJamBearing>() },
            
            { NameTable.TableSuppressSource, new ClassTable<TableSuppressSource>() },
            { NameTable.TableSuppressGnss, new ClassTable<TableSuppressGnss>() },
            
            { NameTable.TableOwnUAV, new ClassTable<TableOwnUAV>() },
            { NameTable.TableOwnUAVFreq, new ClassTable<TableOwnUAVFreq>() },
            { NameTable.TableCuirasseMPoints, new ClassTable<TableCuirasseMPoints>() }
        };

        #region Events
        public event EventHandler<ClientEventArgs> OnConnect = (object obj, ClientEventArgs evenArgs) => { };
        public event EventHandler<ClientEventArgs> OnDisconnect = (object obj, ClientEventArgs evenArgs) => { };
        public event EventHandler<DataEventArgs> OnUpData = (object obj, DataEventArgs evenArgs) => { };
        public event EventHandler<OperationTableEventArgs> OnErrorDataBase = (object obj, OperationTableEventArgs evenArgs) => { };
        #endregion

        private bool ValidEndPoint(string endpointAddress)
        {
            string ValidEndpointRegex = @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b";    // IP validation 
            Regex r = new Regex(ValidEndpointRegex, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match m = r.Match(endpointAddress);
            return m.Success;
        }


        public async void DataCallback(DataEventArgs lData)
        {
            try
            {
                await Task.Run(() =>
                {
                    OnUpData(this, lData);
                    (Tables[lData.Name] as IClickUpData).ClickUpTable(lData.AbstractData);
                }).ConfigureAwait(false);
                return;
            }
            catch (Exception excp)
            {
                OnErrorDataBase(this, new OperationTableEventArgs(NameTableOperation.Update, excp.Message));
                return;
            }
        }


        public async void ErrorCallback(OperationTableEventArgs eventArgs)
        {
            await Task.Run(() =>
            OnErrorDataBase(this, eventArgs)).ConfigureAwait(false);
        }


        public void Abort()
        {
            ClientServiceDB.Abort();
            ((IDisposable)ClientServiceDB).Dispose();
            ClientServiceDB = null;

            foreach (ClassTable table in Tables.Values)
                table.Dispose();
            OnDisconnect(null, new ClientEventArgs(ClientEventArgs.ActServer.Disconnect));
        }


        public async void DataCallback(ServiceDB.DataEventArgs lData)
        {
            try
            {
                await Task.Run(() =>
                {
                    OnUpData(this, new DataEventArgs(lData.Name, lData.AbstractData));
                    (Tables[lData.Name] as IClickUpData).ClickUpTable(lData.AbstractData);
                }).ConfigureAwait(false);
                return;
            }
            catch (Exception excp)
            {
                OnErrorDataBase(this, new OperationTableEventArgs(NameTableOperation.Update, excp.Message));
                return;
            }
        }


        public async void ErrorCallback(ServiceDB.OperationTableEventArgs error)
        {
            await Task.Run(() =>
            OnErrorDataBase(this, new OperationTableEventArgs(error.Operation, error.TypeError))
            ).ConfigureAwait(false);
        }


        public async void RecordCallBack(ServiceDB.RecordEventArgs recordEventArgs)
        {
            await Task.Run(() =>
            (Tables[recordEventArgs.Name] as IClickUpRecord).ClickUpRecord(
                new InheritorsEventArgs.RecordEventArgs(recordEventArgs.Name, recordEventArgs.AbstractRecord, recordEventArgs.NameAction))
            ).ConfigureAwait(false);
        }


        public async void RangeCallBack(ServiceDB.DataEventArgs lData)
        {
            try
            {
                await Task.Run(() =>
                (Tables[lData.Name] as IClickUpAddRange).ClickUpAddRange(lData.AbstractData)).ConfigureAwait(false);
                return;
            }
            catch (Exception excp)
            {
                OnErrorDataBase(this, new OperationTableEventArgs(NameTableOperation.Update, excp.Message));
                return;
            }
        }
    }
}
