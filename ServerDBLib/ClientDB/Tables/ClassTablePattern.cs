﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ClientDataBase.ServiceDB;
using GrozaSModelsDBLib;
using System.ServiceModel;
using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using System.Windows;
using InheritorsEventArgs;
using System.Windows.Media;

namespace ClientDataBase
{
    internal class ClassTablePattern : ClassTable<TablePattern>
    {
        public ClassTablePattern(ref ServiceDBClient clientServiceDB, int id) : base(ref clientServiceDB, id)
        { }

        public ClassTablePattern() : base()
        { }

        #region ITableAddRange

        public override event EventHandler<TableEventArgs<TablePattern>> OnAddRange;

        public override void ClickUpAddRange(ClassDataCommon dataCommon)
        {
            List<GrozaSModelsDBLib.TablePattern> tempList = dataCommon.ToList<GrozaSModelsDBLib.TablePattern>();
            foreach (GrozaSModelsDBLib.TablePattern pattern in tempList as List<GrozaSModelsDBLib.TablePattern>)
            {
                pattern.Image = GetImageStream(ByteArrayToImage(pattern.ImageByte));
                pattern.ImageByte = null;
            }

            OnAddRange?.Invoke(this, new TableEventArgs<TablePattern>(ClassDataCommon.ConvertToListAbstractCommonTable(tempList)));
        }
        #endregion

        #region UpFullData

        public override event EventHandler<TableEventArgs<TablePattern>> OnUpTable;

        public override void ClickUpTable(ClassDataCommon data)
        {
            List<GrozaSModelsDBLib.TablePattern> tempList = data.ToList<GrozaSModelsDBLib.TablePattern>();
            //foreach (GrozaSModelsDBLib.TablePattern pattern in tempList as List<GrozaSModelsDBLib.TablePattern>)
            //{
            //    pattern.Image = GetImageStream(ByteArrayToImage(pattern.ImageByte));
            //    pattern.ImageByte = null;
            //}

            OnUpTable?.Invoke(this, new TableEventArgs<TablePattern>(ClassDataCommon.ConvertToListAbstractCommonTable(tempList)));
        }

        #endregion

        #region UpRecord

        public override event EventHandler<TablePattern> OnAddRecord;

        public override event EventHandler<TablePattern> OnDeleteRecord;

        public override event EventHandler<TablePattern> OnChangeRecord;

        public override void ClickUpRecord(InheritorsEventArgs.RecordEventArgs eventArgs)
        {
            var data = eventArgs.AbstractRecord as TablePattern;
            data.Image = GetImageStream(ByteArrayToImage(data.ImageByte));
            data.ImageByte = null;

            switch (eventArgs.NameAction)
            {
                case NameChangeOperation.Add:
                    OnAddRecord?.Invoke(this, data);
                    break;
                case NameChangeOperation.Change:
                    OnChangeRecord?.Invoke(this, data);
                    break;
                case NameChangeOperation.Delete:
                    OnDeleteRecord?.Invoke(this, data);
                    break;
            }
        }
        #endregion

        public override void Add(object obj)
        {
            try
            {
                TablePattern record = obj as TablePattern;
                //if (record.ImagePath != null && record.ImagePath != String.Empty)
                //{ 
                //    record.ImageByte = ImageToByteArray(Image.FromFile(record.ImagePath)); 
                //}
                if (record.Image != null)
                {
                    record.ImageByte = ImageSourceToBytes(record.Image);
                }

                base.Add(record);
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }


        public override async Task AddAsync(object obj)
        {
            try
            {
                TablePattern record = obj as TablePattern;
                //if (record.ImagePath != null)
                //{
                //    record.ImageByte = ImageToByteArray(Image.FromFile(record.ImagePath));
                //}
                if (record.Image != null)
                {
                    record.ImageByte = ImageSourceToBytes(record.Image);
                }

                await base.AddAsync(record);
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public override void AddRange(object rangeObj)
        {
            try
            {
                List<TablePattern> list = rangeObj as List<TablePattern>;
            
                foreach (TablePattern pattern in list)
                {
                    //if (pattern.ImagePath != null)
                    //{
                    //    pattern.ImageByte = ImageToByteArray(Image.FromFile(pattern.ImagePath));
                    //}
                    if (pattern.Image != null)
                    {
                        pattern.ImageByte = ImageSourceToBytes(pattern.Image);
                    }
                }

                base.AddRange(list);
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public override async Task AddRangeAsync(object rangeObj)
        {
            try
            {
                List<TablePattern> list = rangeObj as List<TablePattern>;

                foreach (TablePattern pattern in list)
                {
                    //if (pattern.ImagePath != null)
                    //{
                    //    pattern.ImageByte = ImageToByteArray(Image.FromFile(pattern.ImagePath));
                    //}
                    if (pattern.Image != null)
                    {
                        pattern.ImageByte = ImageSourceToBytes(pattern.Image);
                    }
                }

                await base.AddRangeAsync(list);
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }


        public override void Change(object obj)
        {
            try
            {
                TablePattern record = obj as TablePattern;
                //if (record.ImagePath != null)
                //{ 
                //    record.ImageByte = ImageToByteArray(Image.FromFile(record.ImagePath)); 
                //}
                if (record.Image != null)
                {
                    record.ImageByte = ImageSourceToBytes(record.Image);
                }

                base.Change(record);
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }


        public override async Task ChangeAsync(object obj)
        {
            try
            {
                TablePattern record = obj as TablePattern;
                //if (record.ImagePath != null)
                //{
                //    record.ImageByte = ImageToByteArray(Image.FromFile(record.ImagePath));
                //}
                if (record.Image != null)
                {
                    record.ImageByte = ImageSourceToBytes(record.Image);
                }

                await base.ChangeAsync(record);
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }


        

        public override List<AbstractCommonTable> Load<AbstractCommonTable>()
        {
            ClassDataCommon Data = new ClassDataCommon();
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }

                Data = ClientServiceDB.LoadData(Name, Id);
                List<TablePattern> tempList = Data.ToList<TablePattern>();
                foreach (TablePattern pattern in tempList as List<TablePattern>)
                {
                    if (pattern.ImageByte == null) continue;
                    pattern.Image = GetImageStream(ByteArrayToImage(pattern.ImageByte));
                    pattern.ImageByte = null;
                    //pattern.ImageByte = null;
                }
                return ClassDataCommon.ConvertToListAbstractCommonTable(tempList).ToList<AbstractCommonTable>();
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (FaultException<InheritorsException.ExceptionWCF> except)
            {
                throw new ExceptionDatabase(except.Detail);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public override async Task<List<AbstractCommonTable>> LoadAsync<AbstractCommonTable>()
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                var result = await ClientServiceDB.LoadDataAsync(Name, Id);

                List<GrozaSModelsDBLib.TablePattern> tempList = result.ToList<GrozaSModelsDBLib.TablePattern>();
                foreach (GrozaSModelsDBLib.TablePattern pattern in tempList as List<GrozaSModelsDBLib.TablePattern>)
                {
                    if (pattern.ImageByte == null) continue;
                    pattern.Image = GetImageStream(ByteArrayToImage(pattern.ImageByte));
                    pattern.ImageByte = null;
                    //pattern.ImageByte = null;
                }
                return ClassDataCommon.ConvertToListAbstractCommonTable(tempList).ToList<AbstractCommonTable>();

            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (FaultException<InheritorsException.ExceptionWCF> except)
            {
                throw new ExceptionDatabase(except.Detail);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }




        #region Image to path converters

        //public byte[] ImageSourceToBytes(ImageSource imageSource)
        //{
        //    StreamImageSource streamImageSource = (StreamImageSource)imageSource;
        //    System.Threading.CancellationToken cancellationToken =
        //    System.Threading.CancellationToken.None;
        //    Task<Stream> task = streamImageSource.Stream(cancellationToken);
        //    Stream stream = task.Result;
        //    byte[] bytesAvailable = new byte[stream.Length];
        //    stream.Read(bytesAvailable, 0, bytesAvailable.Length);
        //    return bytesAvailable;
        //}

        public static byte[] ImageSourceToBytes(ImageSource imageSource)
        {
            var encoder = new PngBitmapEncoder();
            byte[] bytes = null;
            var bitmapSource = imageSource as BitmapSource;

            if (bitmapSource != null)
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }

            return bytes;
        }

        public BitmapSource ConvertToImageSource(byte[] byteImage)
        {
            try
            {
                return GetImageStream(ByteArrayToImage(byteImage));
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }


        private byte[] ImageToByteArray(Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                return ms.ToArray();
            }
        }

        

        private static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            using (var ms = new MemoryStream(byteArrayIn))
            {
                var returnImage = Image.FromStream(ms);

                return returnImage;
            }
        }


        private static BitmapSource GetImageStream(Image myImage)
        {
            var bitmap = new Bitmap(myImage);
            IntPtr bmpPt = bitmap.GetHbitmap();
            BitmapSource bitmapSource =
             System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                   bmpPt,
                   IntPtr.Zero,
                   Int32Rect.Empty,
                   BitmapSizeOptions.FromEmptyOptions());

            //freeze bitmapSource and clear memory to avoid memory leaks
            bitmapSource.Freeze();
            DeleteObject(bmpPt);

            return bitmapSource;
        }

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool DeleteObject(IntPtr value);

        #endregion
    }
}
