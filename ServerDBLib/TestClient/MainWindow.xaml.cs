﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using ClientDataBase;
using GrozaSModelsDBLib;
using InheritorsEventArgs;
using System.Collections.ObjectModel;
using System.Threading;
using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;

namespace TestClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ClientDB clientDB;
        string endPoint = "127.0.0.1:8302";

        public MainWindow()
        {
            InitializeComponent();
            this.Name = "TestClient";

        }


        public void DispatchIfNecessary(Action action)
        {
           if (!Dispatcher.CheckAccess())
               Dispatcher.Invoke(action);
            else
              action.Invoke();
        }


        private void InitClientDB()
        {
            clientDB.OnConnect += ClientDB_OnConnect; ;
            clientDB.OnDisconnect += HandlerDisconnect;
            clientDB.OnUpData += HandlerUpData;
            clientDB.OnErrorDataBase += HandlerErrorDataBase;
            (clientDB.Tables[NameTable.TableSource] as ITableUpdate<TableSource>).OnUpTable += TSource_OnUpTable;
            (clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += MainWindow_OnUpTable; ;
            (clientDB.Tables[NameTable.TableSource] as ITableUpRecord<TableSource>).OnChangeRecord += MainWindow_OnChangeRecord;
            (clientDB.Tables[NameTable.TablePattern] as ITableUpdate<TablePattern>).OnUpTable += TablePattern_OnUpTable;
            (clientDB.Tables[NameTable.TableOwnUAV] as ITableUpdate<TableOwnUAV>).OnUpTable += MainWindow_OnUpTable1; ;
        }

        private void MainWindow_OnUpTable(object sender, TableEventArgs<TableFreqKnown> e)
        {
            //throw new NotImplementedException();
        }

        private void MainWindow_OnUpTable1(object sender, TableEventArgs<TableOwnUAV> e)
        {
            var t = 9;
        }

        private void TablePattern_OnUpTable(object sender, TableEventArgs<TablePattern> e)
        {
            if (e.Table.Count > 0)
            {
                DispatchIfNecessary(() =>
                {
                    Img.Source = (e.Table[0] as TablePattern).Image;
                });
            }
        }

        private void MainWindow_OnChangeRecord(object sender, TableSource e)
        {
            var temp = e;
        }




        #region Handlers
        private void TSource_OnUpTable(object sender, TableEventArgs<TableSource> args)
        {
            foreach (var record in args.Table)
            {
                DispatchIfNecessary(() =>
                        {
                            tbMessage.AppendText($" Id Source = {record.Id} , Track[0].Bearing[0].Bearing = {record.Track[0].Bearing[0].Bearing} \n");

                        });
            }
        }


        private async void ClientDB_OnConnect(object sender, ClientEventArgs e)
        {
            btnCon.Content = "Disconnect";

            foreach (NameTable table in Enum.GetValues(typeof(NameTable)))
            {
                var tempTable = await clientDB.Tables[table].LoadAsync<AbstractCommonTable>();
                DispatchIfNecessary(() =>
                {
                    tbMessage.AppendText($"Load data from Db. {table.ToString()} count records - {tempTable.Count} \n");
                });
            }
        }

        

        void HandlerDisconnect(object obj, ClientEventArgs eventArgs)
        {
            btnCon.Content = "Connect";
            if (eventArgs.GetMessage != "")
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(eventArgs.GetMessage);
            }
            clientDB = null;
        }

        void HandlerUpData(object obj, DataEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Black;

                tbMessage.AppendText($"Load data from Db. Tables {eventArgs.Name.ToString()} count records - {eventArgs.AbstractData.ListRecords.Count} \n");
            });
        }

        void HandlerErrorDataBase(object obj, OperationTableEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(eventArgs.GetMessage);
            });
        }
        #endregion


        #region Buttons
        private void btnCon_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (clientDB != null)
                    clientDB.Disconnect();
                else
                {
                    clientDB = new ClientDB(this.Name, endPoint);
                    InitClientDB();
                    clientDB.ConnectAsync();
                }
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }


        private void ButAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(RecordID.Text);
                Random rand = new Random();
                object record = null;
                switch (Tables.SelectedItem)
                {

                    case NameTable.TableJammerStation:
                        record = new TableJammerStation
                        {
                            Id = nufOfRec,
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = -53.56d,
                                Longitude = -26.365d
                            },
                            //Coordinates = new Coord
                            //{
                            //    Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                            //    Latitude = rand.NextDouble() * rand.Next(0, 360),
                            //    Longitude = rand.NextDouble() * rand.Next(0, 360)
                            //},
                            CallSign = "topol",
                            DeltaTime = "T9",
                            Note = "hello local",
                            Role = StationRole.Linked
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        record = new TableFreqKnown
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqRangesRecon:
                        record = new TableFreqRangesRecon
                        {
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableSectorsRecon:
                        record = new TableSectorsRecon
                        {
                            AngleMin = Convert.ToInt16(rand.Next(0, 360)),
                            AngleMax = Convert.ToInt16(rand.Next(0, 360)),
                            NumberASP = nufOfRec
                        };
                        break;
                    case NameTable.TableSource:
                        record = new TableSource
                        {
                            //Id = rand.Next(1,10),
                            Type = Convert.ToByte(rand.Next(1, 5)),
                            Note = DateTime.Now.ToString(),
                            Track = new ObservableCollection<TableTrack>()
                            {
                                { new TableTrack() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50), FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now,
                                    Bearing = new ObservableCollection<TableJamBearing>()
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 5 },
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 4 } }
                                }
                                } },
                                { new TableTrack() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) , FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now,
                                    Bearing = new ObservableCollection<TableJamBearing>()
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 5 },
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 4 } }
                                }
                                } },
                            },
                        };
                        break;
                    case NameTable.TableTrack:
                        record = new TableTrack
                        {
                            TableSourceId = 21,
                            BandMHz = 9
                        };
                        break;
                    case NameTable.GlobalProperties:
                        record = new GlobalProperties()
                        {
                            Id = 1, 
                            //Spoofing = new Coord() { Latitude = -250 }
                            Gnss = new Coord() { Latitude = -250, Longitude = -20 }
                            //Num = 0,
                            //Coordinates = new Coord() { Altitude = -1, Latitude = -1, Longitude = -1 },
                            //CompassJ = null,
                            //CompassRI = null,
                            //AutoMode = false,
                            //AutoCourse = true,
                            //CourseAngle = -1,
                            //ZoneAttention = 5000,
                            //ZoneReadiness = 3000,
                            //ZoneAlarm = 1500,
                            //TimeRadiat = 3,
                            //Power100_500 = 125,
                            //Power500_2500 = 50,
                            //Power2500_6000 = 100
                        };
                        break;
                    case NameTable.TableSuppressSource:
                        record = new TableSuppressSource()
                        {
                            FrequencyMHz = rand.NextDouble(),
                            LevelOwn = (short)rand.Next(10),
                            TableSourceId = rand.Next(20),
                            InputType = TypeInput.Manual
                        };
                        break;

                    case NameTable.TablePattern:
                        record = new TablePattern()
                        {
                            //ImageByte = ImageToByteArray( ImageByte.FromFile("E:\\KAKORENKO\\WORK\\3_DJI Mavic 2.png"))
                            FrequencyList =  new ObservableCollection<OperatingFrequency>() { new OperatingFrequency() { FrequencyMin = 500, FrequencyMax = 1000, Band = 10 }, new OperatingFrequency() { FrequencyMax = 600, FrequencyMin = 102, Band = 10 } }
                        };
                        break;
                    case NameTable.TableOwnUAV:
                        record = new TableOwnUAV
                        {
                            //Id = rand.Next(1,10),
                            SerialNumber = rand.Next(0,985555).ToString(),
                            Name = "mavic",
                            Note = DateTime.Now.ToString(),
                            Frequencies = new ObservableCollection<TableOwnUAVFreq>()
                            {
                                new TableOwnUAVFreq() { FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now},
                                new TableOwnUAVFreq() { FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now}
                            }
                        };
                        break;
                    case NameTable.TableOwnUAVFreq:
                        record = new TableOwnUAVFreq
                        {
                            FrequencyMHz= rand.NextDouble(), TableOwnUAVId = nufOfRec
                        };
                        break;
                    case NameTable.TableCuirasseMPoints:
                        record = new TableCuirasseMPoints
                        {
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.Next(0, 360),
                                Longitude = rand.Next(0, 360)
                            },
                            Note = rand.Next(0, 360).ToString()
                        };
                        break;
                }
                if (record != null)
                    clientDB?.Tables[(NameTable)Tables.SelectedItem].AddAsync(record);
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }


        private void ButDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(RecordID.Text);
                Random rand = new Random();
                object record = null;
                switch (Tables.SelectedItem)
                {

                    case NameTable.TableJammerStation:
                        record = new TableJammerStation
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        record = new TableFreqKnown
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqRangesRecon:
                        record = new TableFreqRangesRecon
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableSectorsRecon:
                        record = new TableSectorsRecon
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableSource:
                        record = new TableSource
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.GlobalProperties:
                        record = new GlobalProperties
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableSuppressSource:
                        record = new TableSuppressSource
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TablePattern:
                        record = new TablePattern()
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableOwnUAV:
                        record = new TableOwnUAV()
                        {
                            SerialNumber = nufOfRec.ToString()
                        };
                        break;
                    case NameTable.TableCuirasseMPoints:
                        record = new TableCuirasseMPoints()
                        {
                            Id = nufOfRec
                        };
                        break;
                    default:
                        break;
                }
                if (record != null)
                    clientDB?.Tables[(NameTable)Tables.SelectedItem].Delete(record);
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }




        private void SendRange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Random rand = new Random();
                int nufOfRec = Convert.ToInt32(RecordID.Text);
                dynamic listForSend = new List<AbstractCommonTable>();

                switch (Tables.SelectedItem)
                {
                    case NameTable.TableJammerStation:
                        listForSend = new List<TableJammerStation>();
                        for (int i = 0; i < 3; i++)
                        {
                            TableJammerStation record = new TableJammerStation
                            {
                                Coordinates = new Coord
                                {
                                    Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                    Latitude = rand.NextDouble() * rand.Next(0, 360),
                                    Longitude = rand.NextDouble() * rand.Next(0, 360)
                                },
                                CallSign = "topol",
                                //IsOwn = false,
                                Role = StationRole.Own,
                                DeltaTime = "T9",
                                Note = "hello local"
                            };
                            listForSend.Add(record);
                        }
                        break;
                    case NameTable.TableFreqForbidden:
                        listForSend = new List<TableFreqForbidden>();
                        for (int i = 0; i < 3; i++)
                        {
                            TableFreqForbidden record = new TableFreqForbidden
                            {
                                NumberASP = nufOfRec,
                                FreqMaxKHz = rand.Next(150000, 250000),
                                FreqMinKHz = rand.Next(30000, 50000)
                            };
                            listForSend.Add(record);
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        listForSend = new List<TableFreqKnown>();
                        for (int i = 0; i < 3; i++)
                        {
                            TableFreqKnown record = new TableFreqKnown
                            {
                                NumberASP = nufOfRec,
                                FreqMaxKHz = rand.Next(150000, 250000),
                                FreqMinKHz = rand.Next(30000, 50000),
                            };
                            listForSend.Add(record);
                        };
                        break;
                    case NameTable.TableFreqRangesRecon:
                        listForSend = new List<TableFreqRangesRecon>();
                        for (int i = 0; i < 3; i++)
                        {
                            TableFreqRangesRecon record = new TableFreqRangesRecon
                            {
                                NumberASP = nufOfRec,
                                FreqMaxKHz = rand.Next(150000, 250000),
                                FreqMinKHz = rand.Next(30000, 50000),
                            };
                            listForSend.Add(record);
                        };
                        break;
                    case NameTable.TableSectorsRecon:
                        listForSend = new List<TableSectorsRecon>();
                        for (int i = 0; i < 3; i++)
                        {
                            TableSectorsRecon record = new TableSectorsRecon
                            {
                                NumberASP = nufOfRec,
                                AngleMin = Convert.ToInt16(rand.Next(0, 360)),
                                AngleMax = Convert.ToInt16(rand.Next(0, 360)),
                            };
                            listForSend.Add(record);
                        };
                        break;
                    case NameTable.TableSource:
                        listForSend = new List<TableSource>();
                        for (int i = 0; i < 3; i++)
                        {
                            TableSource record = new TableSource
                            {
                                //Id = rand.Next(1,10),
                                Type = Convert.ToByte(rand.Next(1, 5)),
                                Note = DateTime.Now.ToString(),
                                Track = new ObservableCollection<TableTrack>()
                                {
                                    { new TableTrack() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50), FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now,
                                        Bearing = new ObservableCollection<TableJamBearing>()
                                        { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 5 },
                                        { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 4 } }
                                    }
                                    } },
                                    { new TableTrack() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) , FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now,
                                        Bearing = new ObservableCollection<TableJamBearing>()
                                        { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 5 },
                                        { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 4 } }
                                    }
                                    } },
                                },
                            };
                            listForSend.Add(record);
                        };
                        break;
                    case NameTable.TablePattern:
                        listForSend = new List<TablePattern>();
                        for (int i = 0; i < 3; i++)
                        {
                            TablePattern record = new TablePattern
                            {
                                Id = i+1,
                                FrequencyList = new ObservableCollection<OperatingFrequency>() { new OperatingFrequency() { FrequencyMin = 500, FrequencyMax = 1000, Band = 10 }, new OperatingFrequency() { FrequencyMax = 600, FrequencyMin = 102, Band = 10 } }
                            };
                            listForSend.Add(record);
                        };
                        break;
                }
                clientDB?.Tables[(NameTable)Tables.SelectedItem].AddRangeAsync(listForSend);
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }


        private void ButRemove_Click(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();
            int nufOfRec = Convert.ToInt32(RecordID.Text);
            dynamic listForSend = new List<AbstractCommonTable>();

            switch (Tables.SelectedItem)
            {
                case NameTable.TableJammerStation:
                    listForSend = new List<TableJammerStation>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        TableJammerStation record = new TableJammerStation
                        {
                            Id = i
                        };
                        listForSend.Add(record);
                    }
                    break;
                case NameTable.TableFreqForbidden:
                    listForSend = new List<TableFreqForbidden>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        TableFreqForbidden record = new TableFreqForbidden
                        {
                            Id = i
                        };
                        listForSend.Add(record);
                    };
                    break;
                case NameTable.TableFreqKnown:
                    listForSend = new List<TableFreqKnown>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        TableFreqKnown record = new TableFreqKnown
                        {
                            Id = i
                        };
                        listForSend.Add(record);
                    };
                    break;
                case NameTable.TableFreqRangesRecon:
                    listForSend = new List<TableFreqRangesRecon>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        TableFreqRangesRecon record = new TableFreqRangesRecon
                        {
                            Id = i
                        };
                        listForSend.Add(record);
                    };
                    break;
                case NameTable.TableSectorsRecon:
                    listForSend = new List<TableSectorsRecon>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        TableSectorsRecon record = new TableSectorsRecon
                        {
                            Id = i
                        };
                        listForSend.Add(record);
                    };
                    break;
                case NameTable.TableSource:
                    listForSend = new List<TableSource>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        TableSource record = new TableSource
                        {
                            Id = i
                        };
                        listForSend.Add(record);
                    };
                    break;
            }
            if (listForSend != null)
            { clientDB?.Tables[(NameTable)Tables.SelectedItem].RemoveRangeAsync(listForSend); }
        }
        System.Drawing.Bitmap bmp;

        private async void ButLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Black;
                int nufOfRec = Convert.ToInt32(RecordID.Text);

                List<AbstractCommonTable> table = new List<AbstractCommonTable>();
                NameTable nameTable = (NameTable)Tables.SelectedItem;

                if (clientDB.Tables[nameTable] is IDependentAsp)
                {
                    dynamic tablDepenAsp = await (clientDB.Tables[NameTable.TableFreqKnown] as IDependentAsp).LoadByFilterAsync<FreqRanges>(nufOfRec);
                    DispatchIfNecessary(() =>
                    {
                        tbMessage.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()} count records - {tablDepenAsp.Count} \n");
                    });
                }
                else
                {
                    table = await clientDB?.Tables[nameTable].LoadAsync<AbstractCommonTable>();
                    DispatchIfNecessary(() =>
                    {
                        tbMessage.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()} count records - {table.Count} \n");
                    });

                    if (nameTable == NameTable.TablePattern)
                    {
                        //if (table.Count > 0)
                        //{ Img.Source = (table[0] as TablePattern).Image; }
                        if (table.Count > 0)
                        { Img.Source = ImageOperating.ConvertToImageSource((table[0] as TablePattern).ImageByte); }
                        
                    }
                }

                
            }
            catch (ClientDataBase.ExceptionClient exeptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exeptClient.Message);
            }
            catch (ClientDataBase.ExceptionDatabase excpetService)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(excpetService.Message);
            }
        }

        private void ButClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                clientDB?.Tables[(NameTable)Tables.SelectedItem].Clear();
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }
        


        private async void butChange_Click(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();
            int nufOfRec = Convert.ToInt32(RecordID.Text);
            object record = null;
            switch (Tables.SelectedItem)
            {

                case NameTable.TableJammerStation:
                    record = new TableJammerStation
                    {
                        Id = nufOfRec,
                        Coordinates = new Coord
                        {
                            Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                            Latitude = rand.NextDouble() * rand.Next(0, 360),
                            Longitude = rand.NextDouble() * rand.Next(0, 360)
                        },
                        CallSign = "berezka",
                        DeltaTime = "78",
                        Note = "cat",
                        Role = StationRole.Own
                    };
                    break;
                case NameTable.TableFreqForbidden:
                    record = new TableFreqForbidden
                    {
                        Id = nufOfRec, 
                        FreqMaxKHz = rand.Next(150000, 250000),
                        FreqMinKHz = rand.Next(30000, 50000)
                    };
                    break;
                case NameTable.TableFreqKnown:
                    record = new TableFreqKnown
                    {
                        Id = nufOfRec,
                        FreqMaxKHz = rand.Next(150000, 250000),
                        FreqMinKHz = rand.Next(30000, 50000),
                    };
                    break;
                case NameTable.TableFreqRangesRecon:
                    record = new TableFreqRangesRecon
                    {
                        Id = nufOfRec,
                        FreqMaxKHz = rand.Next(150000, 250000),
                        FreqMinKHz = rand.Next(30000, 50000),
                    };
                    break;
                case NameTable.TableSectorsRecon:
                    record = new TableSectorsRecon
                    {
                        Id = nufOfRec,
                        AngleMin = Convert.ToInt16(rand.Next(0, 360)),
                        AngleMax = Convert.ToInt16(rand.Next(0, 360))
                    };
                    break;
                case NameTable.TableSource:
                    record = new TableSource
                    {
                        Id = nufOfRec,
                        Type = Convert.ToByte(rand.Next(1, 5)),
                        Note = DateTime.Now.ToString(),
                        Track = new ObservableCollection<TableTrack>()
                            {
                                { new TableTrack() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50), FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now,
                                    Bearing = new ObservableCollection<TableJamBearing>()
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 5 },
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 4 } }
                                }
                                } },
                                { new TableTrack() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) , FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now,
                                    Bearing = new ObservableCollection<TableJamBearing>()
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 5 },
                                    { new TableJamBearing() { Bearing = (float)rand.NextDouble() * rand.Next(0, 360) , Distance = 100, Level = 10, NumJammer = 4 } }
                                }
                                } },
                            },
                    };
                    break;
                case NameTable.GlobalProperties:
                    record = new GlobalProperties
                    {
                        Id = nufOfRec,
                        //Num = 2
                    };
                    break;
                case NameTable.TableSuppressSource:
                    record = new TableSuppressSource()
                    {
                        Id = nufOfRec,
                        FrequencyMHz = rand.NextDouble(),
                        LevelOwn = (short)rand.Next(10),
                        TableSourceId = rand.Next(20),
                        InputType = TypeInput.Auto
                    };
                    break;
                case NameTable.TableOwnUAV:
                    var table = await clientDB?.Tables[NameTable.TableOwnUAV].LoadAsync<TableOwnUAV>();
                    
                    table[0].Frequencies.Remove(table[0].Frequencies[0]);
                    table[0].Frequencies.Add(new TableOwnUAVFreq() { FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now });
                    record = table[0];
                    //record = new TableOwnUAV
                    //{
                    //    Id = nufOfRec,
                    //    //SerialNumber = nufOfRec.ToString(),
                    //    //Name = "mavic",
                    //    //Note = DateTime.Now.ToString(),
                    //    Frequencies = new ObservableCollection<TableOwnUAVFreq>()
                    //        {
                    //            new TableOwnUAVFreq() { FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now},
                    //            new TableOwnUAVFreq() { FrequencyMHz = rand.NextDouble(), BandMHz = rand.Next(0, 100), Time = DateTime.Now}
                    //        }
                    //};
                    break;
                case NameTable.TablePattern:
                    var patterns = await clientDB?.Tables[NameTable.TablePattern].LoadAsync<TablePattern>();
                    
                    patterns[0].FrequencyList.Remove(patterns[0].FrequencyList[0]);
                    patterns[0].FrequencyList.Add(new OperatingFrequency() { FrequencyMin = rand.NextDouble(), FrequencyMax = rand.NextDouble(), Band = rand.Next() });
                    record = patterns[0];
                    break;
                default:
                    break;
            }
            if (record != null)
                clientDB?.Tables[(NameTable)Tables.SelectedItem].ChangeAsync(record);
        }


        private void butChangeRange_Click(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();
            int nufOfRec = Convert.ToInt32(RecordID.Text);
            dynamic listForSend = new List<AbstractCommonTable>();
            object record = null;
            switch (Tables.SelectedItem)
            {

                case NameTable.TableJammerStation:
                    listForSend = new List<TableJammerStation>();
                    for (int i = nufOfRec; i< nufOfRec +3; i++ )
                    {
                        listForSend.Add(new TableJammerStation
                        {
                            Id = i,
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.NextDouble() * rand.Next(0, 360),
                                Longitude = rand.NextDouble() * rand.Next(0, 360)
                            }
                        });
                    };
                    break;
                case NameTable.TableFreqForbidden:
                    listForSend = new List<TableFreqForbidden>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        listForSend.Add(new TableFreqForbidden
                        {
                            Id = i,
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000)
                        });
                    };
                    break;
               
                default:
                    break;
            }
                clientDB?.Tables[(NameTable)Tables.SelectedItem].ChangeRangeAsync(listForSend);
        }
        #endregion

        private void butTest_Click(object sender, RoutedEventArgs e)
        {
            var tex = new GlobalProperties();
            tex.Gnss.Altitude = 40;
            tex.Gnss = new Coord() {  Altitude =10, Latitude= 10, Longitude= 10 };
        }

        
    }
}
