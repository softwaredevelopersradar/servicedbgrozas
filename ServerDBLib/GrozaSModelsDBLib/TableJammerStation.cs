﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// Станция помех
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder(nameof(Coordinates), 2)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableJammerStation)]
    public class TableJammerStation : AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id)), ReadOnly(false)]
        [Browsable(true)]
        [PropertyOrder(1)]
        public override int Id { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(CallSign))]
        public string CallSign { get; set; }

        [DataMember]
        [Category("Прочее")]
        [Browsable(false)]
        [DisplayName(nameof(DeltaTime))]
        public string DeltaTime { get; set; } = "";

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public String Note { get; set; } = String.Empty;

        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Role))]
        [PropertyOrder(3)]
        public StationRole Role { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(nameof(IsGnssUsed))]
        [PropertyOrder(2)]
        public bool IsGnssUsed { get; set; }


        public override object[] GetKey()
        {
            return new object[] { Id };
        }


        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableJammerStation)record;
            Coordinates.Altitude = newRecord.Coordinates.Altitude;
            Coordinates.Latitude = newRecord.Coordinates.Latitude;
            Coordinates.Longitude = newRecord.Coordinates.Longitude;
            CallSign = newRecord.CallSign;
            DeltaTime = newRecord.DeltaTime;
            Note = newRecord.Note;
            Role = newRecord.Role;
            IsGnssUsed = newRecord.IsGnssUsed;
        }


        public TableJammerStation Clone()
        {
            return new TableJammerStation
            {
                Id = Id,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                CallSign = CallSign,
                DeltaTime = DeltaTime,
                Note = Note,
                Role = Role,
                IsGnssUsed = IsGnssUsed
            };
        }
    }
}
