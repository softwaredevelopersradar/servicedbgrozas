﻿using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// Диапазоны радиоразведки (ДРР)
    /// </summary>
    [DataContract]
    [KnownType(typeof(FreqRanges))]
    [InfoTable(NameTable.TableFreqRangesRecon)]

    public class TableFreqRangesRecon : FreqRanges
    {
    }
}
