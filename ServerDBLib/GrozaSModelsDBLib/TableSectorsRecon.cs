﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// Сектора РР
    /// </summary>
    [DataContract]
    [CategoryOrder("Сектор", 1)]
    [CategoryOrder("Прочее", 2)]
    [KnownType(typeof(AbstractDependentASP))]
    [InfoTable(NameTable.TableSectorsRecon)]
    public class TableSectorsRecon : AbstractDependentASP
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id)), Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(NumberASP)), ReadOnly(true), Browsable(true)]
        public override int NumberASP { get; set; } 

        [DataMember]
        [Category("Сектор")]
        [DisplayName(nameof(AngleMin))]
        [PropertyOrder(1)]
        public short AngleMin { get; set; }

        [DataMember]
        [Category("Сектор")]
        [DisplayName(nameof(AngleMax))]
        [PropertyOrder(2)]
        public short AngleMax { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public String Note { get; set; } = String.Empty;

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(IsActive)), Browsable(false)]
        public bool IsActive { get; set; }

        public TableSectorsRecon() { }
        public TableSectorsRecon(short angleMin, short angleMax, string note, bool isActive, int numberASP, int id = 0)
        {
            AngleMin = angleMin;
            AngleMax = angleMax;
            Note = note;
            IsActive = isActive;
            NumberASP = numberASP;
            Id = id;
        }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableSectorsRecon Clone() => new TableSectorsRecon(AngleMin, AngleMax, Note, IsActive, NumberASP, Id);

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableSectorsRecon)record;
            AngleMin = newRecord.AngleMin;
            AngleMax = newRecord.AngleMax;
            Note = newRecord.Note;
            IsActive = newRecord.IsActive;
        }
    }
}
