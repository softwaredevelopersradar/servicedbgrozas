﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// Траектория ИРИ 
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [KnownType(typeof(TableJamBearing))]
    [InfoTable(NameTable.TableTrack)]
    public class TableTrack : AbstractCommonTable
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        public short Num { get; set; }

        [DataMember]
        public double FrequencyMHz { get; set; }

        [DataMember]
        public double FrequencyRX { get; set; }

        [DataMember]
        public float BandMHz { get; set; }

        [DataMember]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        public float Elevation { get; set; }

        [DataMember]
        public DateTime Time { get; set; } = new DateTime();


        [DataMember]
        public int TableSourceId { get; set; }

        [DataMember]
        public ObservableCollection<TableJamBearing> Bearing { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Num = ((TableTrack)record).Num;
            FrequencyMHz = ((TableTrack)record).FrequencyMHz;
            FrequencyRX = ((TableTrack)record).FrequencyRX;
            BandMHz = ((TableTrack)record).BandMHz;
            Coordinates.Altitude = ((TableTrack)record).Coordinates.Altitude;
            Coordinates.Latitude = ((TableTrack)record).Coordinates.Latitude;
            Coordinates.Longitude = ((TableTrack)record).Coordinates.Longitude;
            Elevation = ((TableTrack)record).Elevation;
            Time = ((TableTrack)record).Time;

            Bearing = Bearing ?? ((TableTrack)record).Bearing;
            if (Bearing != ((TableTrack)record).Bearing)
            {
                Bearing.Clear();
                foreach (var point in ((TableTrack)record).Bearing)
                    Bearing.Add(point);
            }
        }

        public TableTrack Clone()
        {
            return new TableTrack()
            {
                Id = this.Id,
                TableSourceId = this.TableSourceId,
                Num = this.Num,
                FrequencyMHz = this.FrequencyMHz,
                FrequencyRX = FrequencyRX,
                BandMHz = this.BandMHz,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                Elevation = this.Elevation,
                Time = this.Time,
                Bearing = this.Bearing
            };
        }
    }
}
