﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// Частоты своих БПЛА 
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableOwnUAVFreq)]
    public class TableOwnUAVFreq : AbstractCommonTable, INotifyPropertyChanged
    {
        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion

        private double frequencyMHz = 0;
        private float bandMHz = 0;
        private bool isActive;
        private bool sR;

        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        [DisplayName(nameof(FrequencyMHz))]
        [NotifyParentProperty(true)]
        [Category("Frequencies")]
        public double FrequencyMHz
        {
            get { return frequencyMHz; }
            set
            {
                if (frequencyMHz != value)
                {
                    frequencyMHz = value;
                    OnPropertyChanged();

                }
            }
        }
        //public double FrequencyMHz { get; set; }

        [DataMember]
        [DisplayName(nameof(BandMHz))]
        [NotifyParentProperty(true)]
        [Category("Frequencies")]
        public float BandMHz
        {
            get { return bandMHz; }
            set
            {
                if (bandMHz != value)
                {
                    bandMHz = value;
                    OnPropertyChanged();

                }
            }
        }
        //public float BandMHz { get; set; }

        [DataMember]
        [DisplayName(nameof(IsActive))]
        [NotifyParentProperty(true)]
        public bool IsActive
        {
            get { return isActive; }
            set
            {
                if (isActive != value)
                {
                    isActive = value;
                    OnPropertyChanged();
                }
            }
        }


        [DataMember]
        [DisplayName(nameof(SR))]
        [NotifyParentProperty(true)]
        public bool SR
        {
            get { return sR; }
            set
            {
                if (sR != value)
                {
                    sR = value;
                    OnPropertyChanged();
                }
            }
        }

        [DataMember]
        public DateTime Time { get; set; } = new DateTime();

        [DataMember]
        public int TableOwnUAVId { get; set; }

        [DataMember]
        public byte IdSR { get; set; }



        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableOwnUAVFreq)record;

            FrequencyMHz = newRecord.FrequencyMHz;
            BandMHz = newRecord.BandMHz;
            Time = newRecord.Time;
            IsActive = newRecord.IsActive;
            SR = newRecord.SR;
            IdSR = newRecord.IdSR;
        }

        public TableOwnUAVFreq Clone()
        {
            return new TableOwnUAVFreq()
            {
                Id = Id,
                TableOwnUAVId = TableOwnUAVId,
                FrequencyMHz = FrequencyMHz,
                BandMHz = BandMHz,
                Time = Time,
                IsActive = IsActive,
                SR = SR,
                IdSR = IdSR
            };
        }
    }
}
