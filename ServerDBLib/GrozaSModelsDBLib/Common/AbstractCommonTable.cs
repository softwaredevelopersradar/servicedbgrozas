﻿using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    [DataContract]
    public abstract class AbstractCommonTable
    {
        [DataMember]
        public abstract int Id { get; set; }

        public abstract object[] GetKey();

        public abstract void Update(AbstractCommonTable record);
    }
}
