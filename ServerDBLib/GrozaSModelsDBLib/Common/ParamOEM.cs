﻿
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    [DataContract]
    public class ParamOEM : INotifyPropertyChanged
    {
        private int _distance = 7000;

        [DataMember]
        [NotifyParentProperty(true)]
        public int Distance
        {
            get => _distance;
            set
            {
                if (_distance == value) return;
                _distance = value;
                OnPropertyChanged();
            }
        }



        public ParamOEM() { }

        public ParamOEM(int distance)
        {
            Distance = distance;
        }


        #region Model Methods

        public bool EqualTo(ParamOEM model)
        {
            return Distance == model.Distance;
        }

        public ParamOEM Clone() => new ParamOEM(Distance);

        public void Update(ParamOEM model)
        {
            Distance = model.Distance;
        }

        #endregion


        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion
    }
}
