﻿using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    [DataContract]
    public class OperatingFrequency
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public double FrequencyMin { get; set; }

        [DataMember]
        public double FrequencyMax { get; set; }

        [DataMember]
        public float Band { get; set; }
    }
}
