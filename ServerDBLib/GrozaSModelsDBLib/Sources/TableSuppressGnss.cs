﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// GNSS на подавление
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableSuppressGnss)]
    public class TableSuppressGnss : AbstractCommonTable, INotifyPropertyChanged
    {

        private int _id;
        [DataMember]
        [Browsable(false)]
        public override int Id
        {
            get => _id;
            set
            {
                if (_id == value) return;
                _id = value;
                OnPropertyChanged();
            }
        }


        private TypeGNSS _type;
        [DataMember]
        public TypeGNSS Type
        {
            get => _type;
            set
            {
                if (_type == value) return;
                _type = value;
                OnPropertyChanged();
            }
        }

        private bool _l1;
        [DataMember]
        public bool L1
        {
            get => _l1;
            set
            {
                if (_l1 == value) return;
                _l1 = value;
                OnPropertyChanged();
            }
        }

        private bool _l2;
        [DataMember]
        public bool L2
        {
            get => _l2;
            set
            {
                if (_l2 == value) return;
                _l2 = value;
                OnPropertyChanged();
            }
        }

        private bool _l5;
        [DataMember]
        public bool L5
        {
            get => _l5;
            set
            {
                if (_l5 == value) return;
                _l5 = value;
                OnPropertyChanged();
            }
        }
        

        public override object[] GetKey()
        {
            return new object[] { Id };
        }


        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableSuppressGnss)record;

            Type = newRecord.Type;
            L1 = newRecord.L1;
            L2 = newRecord.L2;
            L5 = newRecord.L5;
        }

        public TableSuppressGnss Clone()
        {
            return new TableSuppressGnss()
            {
                Id = Id,
                Type = Type,
                L1 = L1,
                L2 = L2,
                L5 = L5
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
    }
}
