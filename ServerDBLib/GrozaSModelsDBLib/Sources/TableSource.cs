﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;
using System.Collections.ObjectModel;

namespace GrozaSModelsDBLib
{
    /// <summary>
    /// ИРИ 
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(AbstractCommonTable))]
    //[KnownType(typeof(TableTrack))]
    [InfoTable(NameTable.TableSource)]
    public class TableSource : AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(false)]
        public override int Id { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Type))]
        public byte Type { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TypeRSM))]
        public byte TypeRSM { get; set; }


        [DataMember]
        [Browsable(false)]
        public ObservableCollection<TableTrack> Track { get; set; }


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public String Note { get; set; } = String.Empty;


        public override object[] GetKey()
        {
            return new object[] { Id };
        }


        public override void Update(AbstractCommonTable record)
        {
            Type = ((TableSource)record).Type;
            Note = ((TableSource)record).Note;
            TypeRSM = ((TableSource)record).TypeRSM;
            //Track = ((TableSource)record).Track;
            Track = Track ?? ((TableSource)record).Track;
            if (Track != ((TableSource)record).Track)
            {
                Track.Clear();
                foreach (var point in ((TableSource)record).Track)
                    Track.Add(point);
            }
        }

        public TableSource Clone()
        {
            return new TableSource()
            {
                Id = this.Id,
                Type = this.Type,
                TypeRSM = TypeRSM,
                Note = this.Note,
                Track = this.Track
            };
        }
    }



}
