﻿using GrozaSModelsDBLib;

namespace OperationsTablesLib
{
    public interface IDependentAsp
    {
        ClassDataDependASP LoadByFilter(int idClient, int NumberASP);

        void ClearByFilter(int idClient, int NumberASP);
    }
}
