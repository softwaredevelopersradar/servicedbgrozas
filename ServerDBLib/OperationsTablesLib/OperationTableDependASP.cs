﻿using System;
using System.Linq;
using GrozaSModelsDBLib;
using Microsoft.EntityFrameworkCore;
using System.Collections;

namespace OperationsTablesLib
{
    public class OperationTableDependAsp<T> : OperationTableDb<T>, IDependentAsp where T : AbstractDependentASP
    {
        public ClassDataDependASP LoadByFilter(int idClient, int NumberASP)
        {
            ClassDataDependASP data = new ClassDataDependASP();
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);
                    if (DataBase.GetTable<TableJammerStation>(NameTable.TableJammerStation).Find(NumberASP) == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchAspIsAbsent);

                    var properties = typeof(T).GetProperties();
                    foreach (var property in properties)
                    {
                        if (property.PropertyType.GetInterface(typeof(IList).Name) != null && !property.PropertyType.IsArray)
                        {
                            // если найдена, то испльзуем жадную загрузку связных таблиц
                            return ClassDataDependASP.ConvertToDataDependASP(Table.Where(t => (t as T).NumberASP == NumberASP).Include(property.Name).ToList());
                        }
                    }
                    return ClassDataDependASP.ConvertToDataDependASP(Table.Where(t => (t as T).NumberASP == NumberASP).ToList());
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    var recordAsp = DataBase.GetTable<TableJammerStation>(NameTable.TableJammerStation).Find((record as AbstractDependentASP).NumberASP);

                    if (recordAsp == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchAspIsAbsent);
                    }
                }
                base.Add(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    var recordAsp = DataBase.GetTable<TableJammerStation>(NameTable.TableJammerStation).Find((record as AbstractDependentASP).NumberASP);

                    if (recordAsp == null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchAspIsAbsent);
                    }
                }
                base.Delete(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void ClearByFilter(int idClient, int NumberASP)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);
                    if (DataBase.GetTable<TableJammerStation>(NameTable.TableJammerStation).Find(NumberASP) == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchAspIsAbsent);


                    Table.RemoveRange(Table.Where(t => t.NumberASP == NumberASP).ToList());
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
