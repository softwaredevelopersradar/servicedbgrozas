﻿using System.ServiceModel;
using GrozaSModelsDBLib;
using InheritorsEventArgs;
using InheritorsException;

namespace OperationsTablesLib
{
    [ServiceContract(CallbackContract = typeof(ICommonCallback))]
    #region KnownType
    [ServiceKnownType(typeof(ClassDataCommon))]
    [ServiceKnownType(typeof(ClassDataDependASP))]
    [ServiceKnownType(typeof(AbstractCommonTable))]
    [ServiceKnownType(typeof(AbstractDependentASP))]
    [ServiceKnownType(typeof(TableFreqKnown))]
    [ServiceKnownType(typeof(TableFreqRangesRecon))]
    [ServiceKnownType(typeof(TableFreqForbidden))]
    [ServiceKnownType(typeof(FreqRanges))]
    [ServiceKnownType(typeof(TableJammerStation))]
    [ServiceKnownType(typeof(TableSource))]
    [ServiceKnownType(typeof(TableSuppressSource))]
    [ServiceKnownType(typeof(TableTrack))]
    [ServiceKnownType(typeof(TableJamBearing))]
    [ServiceKnownType(typeof(GlobalProperties))]
    [ServiceKnownType(typeof(TableSectorsRecon))]
    [ServiceKnownType(typeof(TableSuppressGnss))]
    [ServiceKnownType(typeof(TablePattern))]
    [ServiceKnownType(typeof(TableOwnUAV))]
    [ServiceKnownType(typeof(TableOwnUAVFreq))]
    [ServiceKnownType(typeof(TableCuirasseMPoints))]

    [ServiceKnownType(typeof(NameTable))]
    [ServiceKnownType(typeof(Languages))]
    [ServiceKnownType(typeof(NameChangeOperation))]
    [ServiceKnownType(typeof(NameTableOperation))]
    [ServiceKnownType(typeof(TypeSource))]
    [ServiceKnownType(typeof(TypeInput))]
    [ServiceKnownType(typeof(StationRole))]
    [ServiceKnownType(typeof(TypeGNSS))]
    [ServiceKnownType(typeof(TSSB))]
    [ServiceKnownType(typeof(Modulation))]
    [ServiceKnownType(typeof(ScanChannels))]

    [ServiceKnownType(typeof(InheritorsException.ExceptionWCF))]
    #endregion

    public interface ICommonTableOperation
    {
        [OperationContract(IsOneWay = true)]
        void ChangeRecord(NameTable nameTable, NameChangeOperation nameAction, AbstractCommonTable record, int IdClient);

        [OperationContract(IsOneWay = true)]
        void AddRangeRecord(NameTable nameTable, ClassDataCommon rangeRecord, int IdClient);

        [OperationContract(IsOneWay = true)]
        void ChangeRangeRecord(NameTable nameTable, ClassDataCommon rangeRecord, int IdClient);

        [OperationContract(IsOneWay = true)]
        void RemoveRangeRecord(NameTable nameTable, ClassDataCommon rangeRecord, int IdClient);

        [OperationContract(IsOneWay = true)]
        void ClearTable(NameTable nameTable, int IdClient);

        [OperationContract]
        [FaultContract(typeof(ExceptionWCF))]
        [ServiceKnownType(typeof(InheritorsException.ExceptionWCF))]
        ///<summary>
        ///Загрузка данных
        ///</summary>
        ClassDataCommon LoadData(NameTable nameTable, int IdClient);

        [OperationContract]
        [FaultContract(typeof(ExceptionWCF))]
        [ServiceKnownType(typeof(InheritorsException.ExceptionWCF))]
        ///<summary>
        ///Загрузка данных из связных таблиц с TableAsp по фильтру - номер Станции помех
        /// </summary>
        ClassDataDependASP LoadDataFilterASP(NameTable nameTable, int NumberASP, int IdClient);

        [OperationContract(IsOneWay = true)]
        ///<summary>
        ///Очистка данных по фильтру
        /// </summary>
        void ClearTableByFilter(NameTable nameTable, int IdFilter, int IdClient);
    }

    public interface ICommonCallback
    {
        /// <summary>
        /// Получены данные из БД
        /// </summary>
        /// <param name="lData">Данные из таблицы</param>
        [OperationContract(IsOneWay = true)]
        void DataCallback(DataEventArgs dataEventArgs);

        [OperationContract(IsOneWay = true)]
        void RecordCallBack(RecordEventArgs recordEventArgs);

        [OperationContract(IsOneWay = true)]
        void RangeCallBack(DataEventArgs dataEventArgs);
    }
}
