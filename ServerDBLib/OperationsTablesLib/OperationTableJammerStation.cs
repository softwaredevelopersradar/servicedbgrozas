﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using GrozaSModelsDBLib;
using Errors;

namespace OperationsTablesLib
{
    public class OperationTableJammerStation : OperationTableDb<TableJammerStation>
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableJammerStation>(Name);
                    if (Table.Find(record.GetKey()) != null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordExist);
                    }

                    if ((record as TableJammerStation).Role == StationRole.Own)
                    {
                        var OtherRecs = Table.Where(rec => (rec as TableJammerStation).Role == StationRole.Own);
                        foreach (var otherRecs in OtherRecs)
                            (otherRecs as TableJammerStation).Role = StationRole.Complex;
                    }

                    if ((record as TableJammerStation).Role == StationRole.Linked)
                    {
                        var OtherRecs = Table.Where(rec => (rec as TableJammerStation).Role == StationRole.Linked);
                        foreach (var otherRecs in OtherRecs)
                            (otherRecs as TableJammerStation).Role = StationRole.Complex;
                    }
                    Table.Add(record as TableJammerStation);
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                if (exception.InnerException != null)
                    throw exception.InnerException;
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                bool hasChangedIsOwn = false;
                bool hasChangedIsLincked = false;

                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableJammerStation>(Name);

                    TableJammerStation rec = Table.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                    hasChangedIsOwn = (record as TableJammerStation).Role == StationRole.Own && rec.Role != StationRole.Own;

                    if (hasChangedIsOwn)
                    {
                        var OtherRecs = Table.Where(t => (t as TableJammerStation).Role == StationRole.Own);
                        foreach (var otherRecs in OtherRecs)
                            (otherRecs as TableJammerStation).Role = StationRole.Complex;
                    }

                    hasChangedIsLincked = (record as TableJammerStation).Role == StationRole.Linked && rec.Role != StationRole.Linked;

                    if (hasChangedIsLincked)
                    {
                        var OtherRecs = Table.Where(t => (t as TableJammerStation).Role == StationRole.Linked);
                        foreach (var otherRecs in OtherRecs)
                            (otherRecs as TableJammerStation).Role = StationRole.Complex;
                    }
                    (rec as AbstractCommonTable).Update(record);
                    Table.Update(rec);
                    DataBase.SaveChanges();



                }
                UpDate(idClient);
                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
