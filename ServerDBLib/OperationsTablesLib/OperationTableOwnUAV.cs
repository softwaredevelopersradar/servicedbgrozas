﻿using System;
using System.Linq;
using Errors;
using GrozaSModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationsTablesLib
{
    public class OperationTableOwnUAV : OperationTableDb<TableOwnUAV>
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                (record as TableOwnUAV).Id = Math.Abs((record as TableOwnUAV).SerialNumber.GetHashCode());
                base.Add(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var record in data.ListRecords)
                {
                    (record as TableOwnUAV).Id = Math.Abs((record as TableOwnUAV).SerialNumber.GetHashCode());
                }
                base.AddRange(data, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                (record as TableOwnUAV).Id = Math.Abs((record as TableOwnUAV).SerialNumber.GetHashCode());
                base.Delete(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void RemoveRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var record in data.ListRecords)
                {
                    (record as TableOwnUAV).Id = Math.Abs((record as TableOwnUAV).SerialNumber.GetHashCode());
                }
                base.RemoveRange(data, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {

                lock (DataBase)
                {
                    DbSet<TableOwnUAV> tableSource = DataBase.GetTable<TableOwnUAV>(Name);

                    TableOwnUAV rec = tableSource.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);


                    tableSource.Remove(rec);
                    DataBase.SaveChanges();
                    (rec as AbstractCommonTable).Update(record);
                    tableSource.Add(rec);
                    DataBase.SaveChanges();
                }

                UpDate(idClient);

                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableOwnUAV>(Name);

                    var temp = ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(t => t.Frequencies).ToList());
                    return temp;
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
