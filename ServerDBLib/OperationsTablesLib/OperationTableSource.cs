﻿using System;
using System.Collections.Generic;
using System.Linq;
using GrozaSModelsDBLib;
using InheritorsEventArgs;
using Microsoft.EntityFrameworkCore;
using Errors;

namespace OperationsTablesLib
{
    public class OperationTableSource : OperationTableDb<TableSource>
    {

        public override ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableSource>(Name);

                    //испльзуем жадную загрузку связных таблиц
                    //return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(t => t.Track.Select(o => o.Bearing)).ToList());
                    return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(t => t.Track).ThenInclude(o => o.Bearing).ToList());
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public ClassDataCommon LoadByFilter(int idClient, int NumberSource)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                lock (DataBase)
                {
                    var Table = DataBase.GetTable<TableSource>(Name);
                    return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Where(t => t.Id == NumberSource).Include(t => t.Track.Select(o => o.Bearing)).ToList());
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {

                List<TableSource> AddRange = new List<TableSource>();
                lock (DataBase)
                {
                    DbSet<TableSource> Table = DataBase.GetTable<TableSource>(Name);
                    foreach (var record in data.ListRecords)
                    {
                        var rec = Table.Find(record.GetKey());
                        if (rec != null)
                        {
                            rec.Update(record);
                            Table.Update(rec);
                            if ((record as TableSource).Track != null)
                            {
                                DbSet<TableTrack> tableTrack = DataBase.TTrack;
                                tableTrack.UpdateRange(rec.Track);
                            }
                            AddRange.Add(rec);
                        }
                        else
                        {
                            Table.Add(record as TableSource);
                            AddRange.Add(record as TableSource);
                        }
                    }
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
                SendRange(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(AddRange)));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        //public override void Change(AbstractCommonTable record, int idClient)
        //{
        //    try
        //    {

        //        lock (DataBase)
        //        {
        //            DbSet<TableSource> Table = DataBase.GetTable<TableSource>(Name);

        //            TableSource rec = Table.Find(record.GetKey());
        //            if (rec == null)
        //                throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);
        //            DataBase.Entry(rec).Collection(x => x.Track).IsModified = false;
        //            (rec as AbstractCommonTable).Update(record);

        //            Table.Update(rec);
        //            ////DataBase.Entry(Table).State = EntityState.Detached;
        //            //DataBase.SaveChanges();


        //            // type = DataBase.Entry(Table).State;

        //            //if (rec.Track != null)
        //            //{
        //            //    DbSet<TableTrack> tableTrack = DataBase.TTrack;
        //            //    tableTrack.RemoveRange(tableTrack.Where(c => c.TableSourceId == rec.Id).ToList());
        //            //    DataBase.SaveChanges();
        //            //}

        //            //if (rec.ListJamDirect != null)
        //            //{
        //            //    DbSet<TableJamDirect> tableJamDirects = DataBase.TJamDirects;
        //            //    tableJamDirects.UpdateRange(rec.ListJamDirect);
        //            //}
        //            //if (rec.Track != null)
        //            //{
        //            //    DbSet<TableTrack> tableTrack = DataBase.TTrack;


        //            //    foreach (TableTrack recTr in rec.Track)
        //            //    {
        //            //        DataBase.Entry(recTr).Property(c => c.Bearing).IsModified = true;
        //            //        if (recTr.Bearing != null)
        //            //        {
        //            //            DbSet<TableJamBearing> tableBearing = DataBase.TJamBearing; 
        //            //            tableBearing.UpdateRange(recTr.Bearing);

        //            //            //DataBase.SaveChangesAsync();

        //            //            //DataBase.SaveChanges();
        //            //        }
        //            //    }
        //            //    //TableTrack recTr = tableTrack.Find(record.GetKey());

        //            //    //DataBase.SaveChangesAsync();
        //            //    tableTrack.UpdateRange(rec.Track);
        //            //    DataBase.SaveChanges();

        //            //    //DataBase.SaveChanges();
        //            //}

        //            if (rec.Track != null)
        //            {
        //                var tableTrack = DataBase.TTrack.Where(c => c.TableSourceId == rec.Id);


        //                foreach (TableTrack recTr in rec.Track)
        //                {
        //                    if (tableTrack.Contains(recTr))
        //                    {
        //                        DataBase.Entry(recTr).Collection(x => x.Bearing).IsModified = false;
        //                        DataBase.TTrack.Update(recTr);
        //                    }
        //                    (recTr as AbstractCommonTable).Update(record);
        //                    //if (recTr.Bearing != null)
        //                    //{
        //                    //    var tableBearing = DataBase.TJamBearing.Where(c => c.TableSourceId == rec.Id && c.TableTrackId==recTr.Id).ToList();
        //                    //    tableBearing = recTr.Bearing.ToList();
        //                    //    //foreach (TableJamBearing bear in recTr.Bearing)
        //                    //    //{
        //                    //    //    DataBase.Entry(bear).State = EntityState.Detached;
        //                    //    //}
        //                    //    //DataBase.SaveChangesAsync();


        //                    //}
        //                    //DataBase.Entry(recTr).Collection(c => c.Bearing).IsModified = false;
        //                    //DataBase.SaveChanges();
        //                }
        //                //TableTrack recTr = tableTrack.Find(record.GetKey());

        //                //DataBase.SaveChangesAsync();
        //                //tableTrack = rec.Track.ToList();
        //                //DataBase.SaveChanges();

        //                //DataBase.SaveChanges();
        //            }

        //            //DataBase.Entry(rec).Collection(x => x.Track).IsModified = false;
        //            Table.Update(rec);
        //            DataBase.Entry(rec).State = EntityState.Detached;
        //            DataBase.SaveChanges();
        //            ////Table.Update(rec);
        //            //DataBase.SaveChanges();
        //        }
        //        UpDate(idClient);

        //        return;
        //    }
        //    catch (InheritorsException.ExceptionLocalDB exception)
        //    {
        //        throw exception;
        //    }
        //    catch (DbUpdateException exDb)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
        //    }
        //}

        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {

                lock (DataBase)
                {
                    DbSet<TableSource> tableSource = DataBase.GetTable<TableSource>(Name);

                    TableSource rec = tableSource.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                    //DataBase.Entry(rec).Collection(x => x.Track).IsModified = false;
                    //foreach (TableTrack track in rec.Track)
                    //    DataBase.Entry(track).Collection(x => x.Bearing).IsModified = false;

                    tableSource.Remove(rec);
                    DataBase.SaveChanges();
                    (rec as AbstractCommonTable).Update(record);
                    tableSource.Add(rec);
                    ////DataBase.SaveChanges();

                    //if (rec.Track != null)
                    //{ 
                    //    var tableTrack = DataBase.TTrack;
                    //    foreach (TableTrack recTr in tableTrack.Where(c => c.TableSourceId == rec.Id).ToList())
                    //    {
                           
                    //        if (recTr.Bearing != null)
                    //        {
                    //            DbSet<TableJamBearing> tableBearing = DataBase.TJamBearing;
                    //            tableBearing.RemoveRange(tableBearing.Where(c => c.TableSourceId == rec.Id && c.TableTrackId == recTr.Id).ToList());
                    //            //DataBase.SaveChanges();
                    //        }
                    //        tableTrack.Remove(recTr);
                    //        //DataBase.SaveChanges();
                    //    }
                    //    //DataBase.SaveChanges();
                    //    tableTrack.AddRange(rec.Track);
                    //    //DataBase.SaveChanges();
                    //}
                    ////Table.Update(rec);
                    DataBase.SaveChanges();
                }

                //if (rec.Track != null)
                //    {
                //        var tableTrack = DataBase.TTrack.Where(c => c.TableSourceId == rec.Id);


                //        foreach (TableTrack recTr in rec.Track)
                //        {
                //            if (tableTrack.Contains(recTr))
                //            {
                //                DataBase.Entry(recTr).Collection(x => x.Bearing).IsModified = false;
                //                DataBase.TTrack.Update(recTr);
                //            }
                //            (recTr as AbstractCommonTable).Update(record);
                //            //if (recTr.Bearing != null)
                //            //{
                //            //    var tableBearing = DataBase.TJamBearing.Where(c => c.TableSourceId == rec.Id && c.TableTrackId==recTr.Id).ToList();
                //            //    tableBearing = recTr.Bearing.ToList();
                //            //    //foreach (TableJamBearing bear in recTr.Bearing)
                //            //    //{
                //            //    //    DataBase.Entry(bear).State = EntityState.Detached;
                //            //    //}
                //            //    //DataBase.SaveChangesAsync();


                //            //}
                //            //DataBase.Entry(recTr).Collection(c => c.Bearing).IsModified = false;
                //            //DataBase.SaveChanges();
                //        }
                //        //TableTrack recTr = tableTrack.Find(record.GetKey());

                //        //DataBase.SaveChangesAsync();
                //        //tableTrack = rec.Track.ToList();
                //        //DataBase.SaveChanges();

                //        //DataBase.SaveChanges();
                //    }

                //    //DataBase.Entry(rec).Collection(x => x.Track).IsModified = false;
                //    Table.Update(rec);
                //    DataBase.Entry(rec).State = EntityState.Detached;
                //    DataBase.SaveChanges();
                //    ////Table.Update(rec);
                //    //DataBase.SaveChanges();
                //}

                UpDate(idClient);

                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        //public override void Change(AbstractCommonTable record, int idClient)
        //{
        //    try
        //    {
        //        lock (DataBase)
        //        {
        //            DbSet<TableSource> Table = DataBase.GetTable<TableSource>(Name);

        //            TableSource rec = Table.Find(record.GetKey());
        //            if (rec == null)
        //                throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

        //            DataBase.Entry(rec).Collection(x => x.Track).IsModified = false;
        //            foreach (TableTrack track in rec.Track)
        //                DataBase.Entry(track).Collection(x => x.Bearing).IsModified = false;
        //            (rec as AbstractCommonTable).Update(record);


        //            if (rec.Track != null)
        //            {
        //                DbSet<TableTrack> tableTrack = DataBase.TTrack;
        //                tableTrack.RemoveRange(tableTrack.Where(c => c.TableSourceId == rec.Id).ToList());
        //                //DataBase.SaveChanges();
        //                tableTrack.AddRange(rec.Track);
        //                //DataBase.SaveChanges();
        //            }
        //            Table.Update(rec);
        //            DataBase.SaveChanges();
        //        }
        //        UpDate(idClient);

        //        return;
        //    }
        //    catch (InheritorsException.ExceptionLocalDB exception)
        //    {
        //        throw exception;
        //    }
        //    catch (DbUpdateException exDb)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
        //    }
        //}

    }
}
